require './metahuman'
class Villain < Metahuman 
	attr_accessor :real_name, :screen_name, :power, :hitpoints, :team, :archenemy

	def initialize(real_name, screen_name, power, hitpoints, team, archenemy)
		@real_name = real_name
		@screen_name = screen_name
		@powers = powers
		@hitpoints = hitpoints
		@team = team
		@archenemy = archenemy
	end

	# CAN ATTACK
	def can_attack(metahuman)

	end

	# ATTACK
	def attack(metahuman, power)

	end
end